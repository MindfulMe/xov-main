[# receive]
### Kullanıcı adınız
XOV Exchange hesabınıza bir başkasından ya da bir borsadan BTS gönderimi için, göndericiye kullanıcı adınızı vermeniz yeterlidir. BTS'ler doğrudan kullanıcı adı belirtilerek gönderilir:

[# deposit-short]
### Kripto para yatır/çek
Başka blokzincirlerinden XOV Exchange hesabınıza (ör. BTC gibi) "coin" aktarmak için ya da TL, dolar gibi fiat para birimleriyle XOV Exchange'de işlem yapabilmek için lütfen aşağıdan bir transfer hizmet sağlayıcısı seçin.
