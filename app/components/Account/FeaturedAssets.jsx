import React, {Component} from "react";
import Icon from "../Icon/Icon";
import {getLogo} from "branding";
import {Link} from "react-router-dom";
import AccountStore from "stores/AccountStore";
import axios from "axios";
import {Apis} from "bitsharesjs-ws";
import {ChainStore} from "bitsharesjs";

var logo = getLogo();

class FeaturedAssets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            xov_usd: "0.00",
            xov_eth: "0.00",
            xov_btc: "0.00",
            xov_balance: "0.00",
            xovbank_balance: "0.00",
            xovbank_usd: "0.00",
            xovbank_eth: "0.00",
            xovbank_btc: "0.00"
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    componentDidMount() {
        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=XOVRST`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        xov_btc: dataJson["OPEN.BTC"].price
                    });
                    this.setState({
                        xov_usd: dataJson["USD"].price
                    });
                    this.setState({
                        xov_eth: dataJson["OPEN.ETH"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=XOVBANK`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        xovbank_eth: dataJson["OPEN.ETH"].price
                    });
                    this.setState({
                        xovbank_usd: dataJson["USD"].price
                    });
                    this.setState({
                        xovbank_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });
        setTimeout(() => {
            this.setState({
                xov_balance: localStorage.getItem("xovrst_balance") || "0.00"
            });
            this.setState({
                xovbank_balance:
                    localStorage.getItem("xovbank_balance") || "0.00"
            });
        }, 3000);
    }
    render() {
        return (
            <div className="grid-container">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "95%",
                        margin: "0 auto"
                    }}
                >
                    <div
                        style={{
                            backgroundColor: "white",
                            height: "35px",
                            marginTop: "45px",
                            marginBottom: "-2px",
                            lineHeight: "40px",
                            padding: "0 30px",
                            fontSize: "18px",
                            overflow: "hidden",
                            whiteSpace: "nowrap"
                        }}
                    >
                        {this.props.title}
                    </div>
                    {this.props.sectionName && (
                        <div
                            style={{
                                fontSize: "28px",
                                color: "white",
                                margin: "25px 0"
                            }}
                        >
                            Digital Assets Dashboard
                        </div>
                    )}
                </div>
                <div style={{display: "flex", margin: "-25px 0px -25px 240px"}}>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/xov.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/dollar.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/gdex.btc.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/eth.png`}
                        />
                        &nbsp;
                    </span>
                </div>
                <div
                    style={{
                        backgroundColor: "white",
                        width: "100%",
                        margin: "0 auto",
                        height: this.props.modalHeight
                    }}
                >
                    {(this.state.value === "" ||
                        this.state.value.search(/x|o|v/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={logo} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                XOV
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            >
                                <Icon
                                    size="1_5x"
                                    name="info-filled"
                                    title="XOV is building a decentralized retail banking platform and developing an academically researched stable coin."
                                />
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "150px",
                                    textOverflow: "",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "150px",
                                    textOverflow: "",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "150px",
                                    textOverflow: "",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "150px",
                                    textOverflow: "",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/XOVBANK_BTS"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/x|o|v/gi) == 0) &&
                        !this.props.limited && (
                            <div
                                style={{
                                    display: "inline-grid",
                                    padding: "35px 35px 0px 35px",
                                    gridColumnGap: "10px",
                                    gridTemplateColumns:
                                        "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                                }}
                            >
                                <img
                                    style={{margin: 0, height: 35}}
                                    src={logo}
                                />
                                <div
                                    style={{
                                        textAlign: "center",
                                        fontSize: "18px",
                                        lineHeight: "35px"
                                    }}
                                >
                                    RST
                                </div>
                                <div style={{lineHeight: "46px"}}>
                                    <Icon
                                        size="1_5x"
                                        name="info-filled"
                                        title="XOV is building a decentralized retail banking platform and developing an academically researched stable coin."
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "150px",
                                        textOverflow: "",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_balance}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "150px",
                                        textOverflow: "",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_usd}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "150px",
                                        textOverflow: "",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_btc}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "150px",
                                        textOverflow: "fade",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_eth}
                                </div>
                            </div>
                        )}
                </div>
            </div>
        );
    }
}

export default FeaturedAssets;
