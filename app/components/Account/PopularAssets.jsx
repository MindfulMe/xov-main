import React, {Component} from "react";
import Icon from "../Icon/Icon";
import {
    getLogo,
    getBtc,
    getEth,
    getBts,
    getEos,
    getUsd,
    getEur,
    getCny,
    getRuble,
    getGold,
    getSilver
} from "branding";
import {Link} from "react-router-dom";
import AccountStore from "stores/AccountStore";
import axios from "axios";
import {Apis} from "bitsharesjs-ws";
import {ChainStore} from "bitsharesjs";

var logo = getLogo();
var btc = getBtc();
var bts = getBts();
var eth = getEth();
var eos = getEos();
var usd = getUsd();
var eur = getEur();
var cny = getCny();
var ruble = getRuble();
var gold = getGold();
var silver = getSilver();

class FeaturedAssets extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            btc_eth: 0,
            eth_btc: 0,
            btc_usd: 0,
            eth_usd: 0,
            bts_usd: 0,
            bts_eth: 0,
            bts_btc: 0,
            eos_usd: 0,
            eos_eth: "0.00",
            eos_btc: 0,
            xov_balance: "0.00",
            usd_balance: "0.00",
            eur_balance: "0.00",
            cny_balance: "0.00",
            ruble_balance: "0.00",
            gold_balance: "0.00",
            silver_balance: "0.00",
            xovbank_balance: "0.00",
            eth_balance: "0.00",
            btc_balance: "0.00",
            eos_balance: "0.00",
            bts_balance: "0.00",
            xov_usd: "0.00",
            xov_eth: "0.00",
            xov_btc: "0.00",
            xovbank_usd: "0.00",
            xovbank_eth: "0.00",
            xovbank_btc: "0.00",
            silver_usd: "0.00",
            silver_btc: "0.00",
            silver_eth: "0.00",
            gold_usd: "0.00",
            gold_btc: "0.00",
            gold_eth: "0.00",
            ruble_usd: "0.00",
            ruble_btc: "0.00",
            ruble_eth: "0.00",
            cny_usd: "0.00",
            cny_btc: "0.00",
            cny_eth: "0.00",
            eur_usd: "0.00",
            eur_btc: "0.00",
            eur_eth: "0.00",
            usd_btc: "0.00",
            usd_eth: "0.00"
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    componentDidMount() {
        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=BTS`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                this.setState({
                    bts_usd: dataJson["USD"].price,
                    bts_eth: dataJson["OPEN.ETH"].price,
                    bts_btc: dataJson["OPEN.BTC"].price
                });
            })
            .then(() => {
                axios
                    .get(
                        `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=OPEN.BTC`,
                        {headers: ["origin", "x-requested-with"]}
                    )
                    .then(data => data.data)
                    .then(dataJson => {
                        this.setState({
                            btc_usd: dataJson["USD"].price,
                            btc_eth: dataJson["OPEN.ETH"].price
                        });
                    })
                    .then(() => {
                        axios
                            .get(
                                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=OPEN.ETH`,
                                {headers: ["origin", "x-requested-with"]}
                            )
                            .then(data => data.data)
                            .then(dataJson => {
                                this.setState({
                                    eth_btc: dataJson["OPEN.BTC"].price
                                });
                            })
                            .then(() =>
                                this.setState({
                                    eth_usd:
                                        this.state.btc_usd * this.state.eth_btc
                                })
                            );
                    });
            });
        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=OPEN.EOS`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                this.setState({
                    eos_usd: dataJson["USD"].price,
                    eos_btc: dataJson["OPEN.BTC"].price
                });
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=XOVRST`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        xov_eth: dataJson["OPEN.ETH"].price
                    });
                    this.setState({
                        xov_usd: dataJson["USD"].price
                    });
                    this.setState({
                        xov_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=XOVBANK`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        xovbank_eth: dataJson["OPEN.ETH"].price
                    });
                    this.setState({
                        xovbank_btc: dataJson["OPEN.BTC"].price
                    });
                    this.setState({
                        xovbank_usd: dataJson["USD"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=USD`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        usd_eth: dataJson["OPEN.ETH"].price,
                        usd_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=EUR`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        eur_usd: dataJson["USD"].price,
                        eur_eth: dataJson["OPEN.ETH"].price,
                        eur_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=CNY`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        cny_usd: dataJson["USD"].price,
                        cny_eth: dataJson["OPEN.ETH"].price,
                        cny_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=RUBLE`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        ruble_usd: dataJson["USD"].price,
                        ruble_eth: dataJson["OPEN.ETH"].price,
                        ruble_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=GOLD`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        gold_usd: dataJson["USD"].price,
                        gold_eth: dataJson["OPEN.ETH"].price,
                        gold_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        axios
            .get(
                `${"https://stormy-escarpment-21788.herokuapp.com/"}https://cryptofresh.com/api/asset/markets?asset=SILVER`,
                {headers: ["origin", "x-requested-with"]}
            )
            .then(data => data.data)
            .then(dataJson => {
                if (dataJson) {
                    this.setState({
                        silver_usd: dataJson["USD"].price,
                        silver_eth: dataJson["OPEN.ETH"].price,
                        silver_btc: dataJson["OPEN.BTC"].price
                    });
                }
            });

        setTimeout(() => {
            this.setState({
                xov_balance: localStorage.getItem("xovrst_balance") || "0.00"
            });
            this.setState({
                xovbank_balance:
                    localStorage.getItem("xovbank_balance") || "0.00"
            });
            this.setState({
                eth_balance: localStorage.getItem("eth_balance") || "0.00"
            });
            this.setState({
                btc_balance: localStorage.getItem("btc_balance") || "0.00"
            });
            this.setState({
                eos_balance: localStorage.getItem("eos_balance") || "0.00"
            });
            this.setState({
                bts_balance: localStorage.getItem("bts_balance") || "0.00"
            });
            this.setState({
                usd_balance: localStorage.getItem("usd_balance") || "0.00"
            });
            this.setState({
                eur_balance: localStorage.getItem("eur_balance") || "0.00"
            });
            this.setState({
                ruble_balance: localStorage.getItem("ruble_balance") || "0.00"
            });
            this.setState({
                cny_balance: localStorage.getItem("cny_balance") || "0.00"
            });
            this.setState({
                silver_balance: localStorage.getItem("silver_balance") || "0.00"
            });
            this.setState({
                gold_balance: localStorage.getItem("gold_balance") || "0.00"
            });
        }, 3000);
    }

    render() {
        return (
            <div className="grid-container">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "95%",
                        margin: "0 auto"
                    }}
                >
                    <div
                        style={{
                            backgroundColor: "white",
                            height: "35px",
                            marginTop: "45px",
                            marginBottom: "-2px",
                            lineHeight: "40px",
                            padding: "0 30px",
                            fontSize: "18px"
                        }}
                    >
                        {this.props.title}
                    </div>
                    {this.props.sectionName && (
                        <div
                            style={{
                                fontSize: "28px",
                                color: "white",
                                margin: "25px 0"
                            }}
                        >
                            Digital Assets Dashboard
                        </div>
                    )}
                </div>
                <div style={{display: "flex", margin: "-25px 0px -25px 240px"}}>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/xov.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/dollar.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/gdex.btc.png`}
                        />
                        &nbsp;
                    </span>
                    <span>
                        <img
                            style={{
                                maxWidth: 50,
                                marginRight: 105
                            }}
                            src={`${__BASE_URL__}asset-symbols/eth.png`}
                        />
                        &nbsp;
                    </span>
                </div>
                {this.props.isSearch && (
                    <div>
                        <input
                            onChange={this.handleChange}
                            value={this.state.value}
                            style={{
                                float: "right",
                                margin: "-35px 3% 0px 0px",
                                backgroundColor: "#C7C7C7"
                            }}
                        />
                        <div
                            style={{float: "right", margin: "-30px 4% 0px 0px"}}
                        >
                            <Icon size="1x" name="search" />
                        </div>
                    </div>
                )}
                <div
                    style={{
                        backgroundColor: "white",
                        width: "100%",
                        margin: "0 auto",
                        height: this.props.modalHeight
                    }}
                >
                    {(this.state.value === "" ||
                        this.state.value.search(/x|o|v/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={logo} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                XOV
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            >
                                <Icon
                                    size="1_5x"
                                    name="info-filled"
                                    title="XOV is building a decentralized retail banking platform and developing an academically researched stable coin."
                                />
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.xovbank_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/XOVBANK_BTS"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/r|s|t/gi) == 0) &&
                        !this.props.limited && (
                            <div
                                style={{
                                    display: "inline-grid",
                                    padding: "35px 35px 0px 35px",
                                    gridColumnGap: "10px",
                                    gridTemplateColumns:
                                        "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                                }}
                            >
                                <img
                                    style={{margin: 0, height: 35}}
                                    src={logo}
                                />
                                <div
                                    style={{
                                        textAlign: "center",
                                        fontSize: "18px",
                                        lineHeight: "35px"
                                    }}
                                >
                                    RST
                                </div>
                                <div
                                    style={{
                                        lineHeight: "46px"
                                    }}
                                >
                                    <Icon
                                        size="1_5x"
                                        name="info-filled"
                                        title="XOV is building a decentralized retail banking platform and developing an academically researched stable coin."
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "147px",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_balance}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "147px",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_usd}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "147px",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_btc}
                                </div>
                                <div
                                    style={{
                                        fontSize: "18px",
                                        lineHeight: "35px",
                                        overflow: "hidden",
                                        maxWidth: "147px",
                                        overflow: "hidden"
                                    }}
                                >
                                    {this.state.xov_eth}
                                </div>
                            </div>
                        )}

                    {(this.state.value === "" ||
                        this.state.value.search(/b|t|c/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={btc} />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    textAlign: "center"
                                }}
                            >
                                BTC
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.btc_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.btc_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                1.0
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.btc_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/OPEN.BTC_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}

                    {(this.state.value === "" ||
                        this.state.value.search(/e|t|h/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={eth} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                ETH
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eth_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px"
                                }}
                            >
                                {this.state.eth_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eth_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                1.0
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/OPEN.ETH_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}

                    {(this.state.value === "" ||
                        this.state.value.search(/b|t|s/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={bts} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                BTS
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.bts_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.bts_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.bts_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.bts_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/BTS_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/e|o|s/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={eos} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                EOS
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eos_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eos_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eos_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eos_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/EOS_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/u|s|d/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={usd} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                USD
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.usd_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                1.0
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.usd_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.usd_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/USD_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/e|u|r/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={eur} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                EUR
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eur_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eur_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eur_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.eur_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/EUR_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/c|n|y/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={cny} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                CNY
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.cny_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.cny_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.cny_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.cny_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/CNY_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/r|u|b|l|e/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={ruble} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                RUBLE
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.ruble_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.ruble_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.ruble_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.ruble_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/RUBLE_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/g|o|l|d/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={gold} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                GOLD
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.gold_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.gold_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.gold_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.gold_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/GOLD_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                    {(this.state.value === "" ||
                        this.state.value.search(/s|i|l|v|e|r/gi) == 0) && (
                        <div
                            style={{
                                display: "inline-grid",
                                padding: "35px 35px 0px 35px",
                                gridColumnGap: "10px",
                                gridTemplateColumns:
                                    "50px 50px 80px 150px 150px 150px 150px auto auto auto"
                            }}
                        >
                            <img style={{margin: 0, height: 35}} src={silver} />
                            <div
                                style={{
                                    textAlign: "center",
                                    fontSize: "18px",
                                    lineHeight: "35px"
                                }}
                            >
                                SILVER
                            </div>
                            <div
                                style={{
                                    lineHeight: "46px"
                                }}
                            />
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.silver_balance}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.silver_usd}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.silver_btc}
                            </div>
                            <div
                                style={{
                                    fontSize: "18px",
                                    lineHeight: "35px",
                                    overflow: "hidden",
                                    maxWidth: "147px",
                                    overflow: "hidden"
                                }}
                            >
                                {this.state.silver_eth}
                            </div>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-medium"
                            >
                                <div>DEPOSIT</div>
                            </Link>
                            <Link
                                to="/deposit-withdraw"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>WITHDRAW</div>
                            </Link>
                            <Link
                                to="/market/SILVER_XOVBANK"
                                style={{
                                    color: "black",
                                    border: "solid",
                                    borderColor: "#3071b3",
                                    backgroundColor: "#F6F6F6",
                                    borderRadius: "10px",
                                    borderWidth: "2px",
                                    padding: "10px 40px",
                                    height: "40px"
                                }}
                                className="column-hide-small"
                            >
                                <div>TRADE</div>
                            </Link>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default FeaturedAssets;
