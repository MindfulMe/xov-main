import React from "react";
import {connect} from "alt-react";
import classNames from "classnames";
import AccountActions from "actions/AccountActions";
import AccountStore from "stores/AccountStore";
import AccountNameInput from "./../Forms/AccountNameInput";
import WalletDb from "stores/WalletDb";
import notify from "actions/NotificationActions";
import {Link} from "react-router-dom";
import AccountSelect from "../Forms/AccountSelect";
import TransactionConfirmStore from "stores/TransactionConfirmStore";
import LoadingIndicator from "../LoadingIndicator";
import Translate from "react-translate-component";
import counterpart from "counterpart";
import {ChainStore, FetchChain, key} from "bitsharesjs";
import ReactTooltip from "react-tooltip";
import utils from "common/utils";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import Icon from "../Icon/Icon";
import CopyButton from "../Utility/CopyButton";
import {withRouter} from "react-router-dom";
import {scroller} from "react-scroll";
import Select from "react-select";

let init = () => {
    let years = [];

    let max = new Date().getFullYear() - 1900;

    for (let i = max; i > 0; i--) {
        years[max - i] = {value: 1900 + i, label: 1900 + i};
    }
    return years;
};

const optionsCountry = [
    {value: "1", label: "Afghanistan"},
    {value: "2", label: "Albania"},
    {value: "3", label: "Algeria"},
    {value: "4", label: "Andorra"},
    {value: "5", label: "Angola"},
    {value: "6", label: "Antigua & Deps"},
    {value: "7", label: "Argentina"},
    {value: "8", label: "Armenia"},
    {value: "9", label: "Australia"},
    {value: "10", label: "Austria"},
    {value: "11", label: "Azerbaijan"},
    {value: "12", label: "Bahamas"},
    {value: "13", label: "Bahrain"},
    {value: "14", label: "Bangladesh"},
    {value: "15", label: "Barbados"},
    {value: "16", label: "Belarus"},
    {value: "17", label: "Belgium"},
    {value: "18", label: "Belize"},
    {value: "19", label: "Benin"},
    {value: "20", label: "Bhutan"},
    {value: "21", label: "Bolivia"},
    {value: "22", label: "Bosnia Herzegovina"},
    {value: "23", label: "Botswana"},
    {value: "24", label: "Brazil"},
    {value: "25", label: "Brunei"},
    {value: "26", label: "Bulgaria"},
    {value: "27", label: "Burkina"},
    {value: "28", label: "Burundi"},
    {value: "29", label: "Cambodia"},
    {value: "30", label: "Cameroon"},
    {value: "31", label: "Canada"},
    {value: "32", label: "Cape Verde"},
    {value: "33", label: "Central African Rep"},
    {value: "34", label: "Chad"},
    {value: "35", label: "Chile"},
    {value: "36", label: "China"},
    {value: "37", label: "Colombia"},
    {value: "38", label: "Comoros"},
    {value: "39", label: "Congo"},
    {value: "40", label: "Congo {Democratic Rep}"},
    {value: "41", label: "Costa Rica"},
    {value: "42", label: "Croatia"},
    {value: "43", label: "Cuba"},
    {value: "44", label: "Cyprus"},
    {value: "45", label: "Czech Republic"},
    {value: "46", label: "Denmark"},
    {value: "47", label: "Djibouti"},
    {value: "48", label: "Dominica"},
    {value: "49", label: "Dominican Republic"},
    {value: "50", label: "East Timor"},
    {value: "51", label: "Ecuador"},
    {value: "52", label: "Egypt"},
    {value: "53", label: "El Salvador"},
    {value: "54", label: "Equatorial Guinea"},
    {value: "55", label: "Eritrea"},
    {value: "56", label: "Estonia"},
    {value: "57", label: "Ethiopia"},
    {value: "58", label: "Fiji"},
    {value: "59", label: "Finland"},
    {value: "60", label: "France"},
    {value: "61", label: "Gabon"},
    {value: "62", label: "Gambia"},
    {value: "63", label: "Georgia"},
    {value: "64", label: "Germany"},
    {value: "65", label: "Ghana"},
    {value: "66", label: "Greece"},
    {value: "67", label: "Grenada"},
    {value: "68", label: "Guatemala"},
    {value: "69", label: "Guinea"},
    {value: "70", label: "Guinea-Bissau"},
    {value: "71", label: "Guyana"},
    {value: "72", label: "Haiti"},
    {value: "73", label: "Honduras"},
    {value: "74", label: "Hungary"},
    {value: "75", label: "Iceland"},
    {value: "76", label: "India"},
    {value: "77", label: "Indonesia"},
    {value: "78", label: "Iran"},
    {value: "79", label: "Iraq"},
    {value: "80", label: "Ireland {Republic}"},
    {value: "81", label: "Israel"},
    {value: "82", label: "Italy"},
    {value: "83", label: "Ivory Coast"},
    {value: "84", label: "Jamaica"},
    {value: "85", label: "Japan"},
    {value: "86", label: "Jordan"},
    {value: "87", label: "Kazakhstan"},
    {value: "88", label: "Kenya"},
    {value: "89", label: "Kiribati"},
    {value: "90", label: "Korea North"},
    {value: "91", label: "Korea South"},
    {value: "92", label: "Kosovo"},
    {value: "93", label: "Kuwait"},
    {value: "94", label: "Kyrgyzstan"},
    {value: "95", label: "Laos"},
    {value: "96", label: "Latvia"},
    {value: "97", label: "Lebanon"},
    {value: "98", label: "Lesotho"},
    {value: "99", label: "Liberia"},
    {value: "100", label: "Libya"},
    {value: "101", label: "Liechtenstein"},
    {value: "102", label: "Lithuania"},
    {value: "103", label: "Luxembourg"},
    {value: "104", label: "Macedonia"},
    {value: "105", label: "Madagascar"},
    {value: "106", label: "Malawi"},
    {value: "107", label: "Malaysia"},
    {value: "108", label: "Maldives"},
    {value: "109", label: "Mali"},
    {value: "111", label: "Malta"},
    {value: "112", label: "Marshall Islands"},
    {value: "113", label: "Mauritania"},
    {value: "114", label: "Mauritius"},
    {value: "115", label: "Mexico"},
    {value: "116", label: "Micronesia"},
    {value: "117", label: "Moldova"},
    {value: "118", label: "Monaco"},
    {value: "119", label: "Mongolia"},
    {value: "120", label: "Montenegro"},
    {value: "121", label: "Morocco"},
    {value: "122", label: "Mozambique"},
    {value: "123", label: "Myanmar, {Burma}"},
    {value: "124", label: "Namibia"},
    {value: "125", label: "Nauru"},
    {value: "126", label: "Nepal"},
    {value: "127", label: "Netherlands"},
    {value: "128", label: "New Zealand"},
    {value: "129", label: "Nicaragua"},
    {value: "130", label: "Niger"},
    {value: "131", label: "Nigeria"},
    {value: "132", label: "Norway"},
    {value: "133", label: "Oman"},
    {value: "134", label: "Pakistan"},
    {value: "135", label: "Palau"},
    {value: "136", label: "Panama"},
    {value: "137", label: "Papua New Guinea"},
    {value: "138", label: "Paraguay"},
    {value: "139", label: "Peru"},
    {value: "140", label: "Philippines"},
    {value: "141", label: "Poland"},
    {value: "142", label: "Portugal"},
    {value: "143", label: "Qatar"},
    {value: "144", label: "Romania"},
    {value: "145", label: "Russian Federation"},
    {value: "146", label: "Rwanda"},
    {value: "147", label: "St Kitts & Nevis"},
    {value: "148", label: "St Lucia"},
    {value: "149", label: "Saint Vincent & the Grenadines"},
    {value: "150", label: "Samoa"},
    {value: "151", label: "San Marino"},
    {value: "152", label: "Sao Tome & Principe"},
    {value: "153", label: "Saudi Arabia"},
    {value: "154", label: "Senegal"},
    {value: "155", label: "Serbia"},
    {value: "156", label: "Seychelles"},
    {value: "157", label: "Sierra Leone"},
    {value: "158", label: "Singapore"},
    {value: "159", label: "Slovakia"},
    {value: "160", label: "Slovenia"},
    {value: "161", label: "Solomon Islands"},
    {value: "162", label: "Somalia"},
    {value: "163", label: "South Africa"},
    {value: "164", label: "South Sudan"},
    {value: "165", label: "Spain"},
    {value: "166", label: "Sri Lanka"},
    {value: "167", label: "Sudan"},
    {value: "168", label: "Suriname"},
    {value: "169", label: "Swaziland"},
    {value: "170", label: "Sweden"},
    {value: "171", label: "Switzerland"},
    {value: "172", label: "Syria"},
    {value: "173", label: "Tajikistan"},
    {value: "174", label: "Tanzania"},
    {value: "175", label: "Thailand"},
    {value: "176", label: "Togo"},
    {value: "177", label: "Tonga"},
    {value: "178", label: "Trinidad & Tobago"},
    {value: "179", label: "Tunisia"},
    {value: "180", label: "Turkey"},
    {value: "181", label: "Turkmenistan"},
    {value: "182", label: "Tuvalu"},
    {value: "183", label: "Uganda"},
    {value: "184", label: "Ukraine"},
    {value: "185", label: "United Arab Emirates"},
    {value: "186", label: "United Kingdom"},
    {value: "187", label: "United States"},
    {value: "188", label: "Uruguay"},
    {value: "189", label: "Uzbekistan"},
    {value: "190", label: "Vanuatu"},
    {value: "191", label: "Vatican City"},
    {value: "192", label: "Venezuela"},
    {value: "193", label: "Vietnam"},
    {value: "194", label: "Yemen"},
    {value: "195", label: "Zambia"},
    {value: "196", label: "Zimbabwe"}
];

const optionsAge = init();

const optionsVip = [{value: "yes", label: "Yes"}, {value: "no", label: "No"}];

const selectorStyles = {
    indicatorsContainer: (base, state) => ({
        ...base,
        backgroundColor: "#3071b3",
        color: "white"
    }),
    indicatorSeparator: (base, state) => ({
        ...base,
        width: 0
    }),
    valueContainer: (base, state) => ({
        ...base,
        backgroundColor: "#3071b3",
        height: "37px",
        minWidth: "200px",
        color: "white"
    }),
    placeholder: (base, state) => ({
        ...base,
        color: "white"
    }),
    singleValue: (base, state) => ({
        ...base,
        color: "white"
    })
};

class CreateAccountPassword extends React.Component {
    constructor() {
        super();
        this.state = {
            validAccountName: false,
            accountName: "",
            validPassword: true,
            registrar_account: null,
            loading: false,
            hide_refcode: true,
            show_identicon: false,
            step: 1,
            showPass: false,
            generatedPassword: ("P" + key.get_random_key().toWif()).substr(
                0,
                45
            ),
            confirm_password: "",
            understand_1: false,
            understand_2: false,
            understand_3: false,
            currentStep: 1,
            selectedOptionCountry: {value: "1", label: "Afghanistan"},
            selectedOptionAge: {value: ""},
            selectedOptionVip: null,
            firstname: "",
            surname: "",
            nickname: "",
            understand_4: false,
            isNextStep: false,
            infoDescription: false,
            infoCountry: false,
            infoBorn: false,
            infoVip: false,
            infoName: false,
            infoNick: false,
            infoPass: false
        };
        this.onFinishConfirm = this.onFinishConfirm.bind(this);

        this.accountNameInput = null;

        this.scrollToInput = this.scrollToInput.bind(this);
    }

    componentWillMount() {
        if (!WalletDb.getWallet()) {
            SettingsActions.changeSetting({
                setting: "passwordLogin",
                value: true
            });
        }
    }

    componentDidMount() {
        ReactTooltip.rebuild();
        this.scrollToInput();
        this.setState({confirm_password: this.state.generatedPassword});
    }

    shouldComponentUpdate(nextProps, nextState) {
        return !utils.are_equal_shallow(nextState, this.state);
    }

    scrollToInput() {
        scroller.scrollTo(`scrollToInput`, {
            duration: 1500,
            delay: 100,
            smooth: true,
            containerId: "accountForm"
        });
    }

    isValid() {
        let firstAccount = AccountStore.getMyAccounts().length === 0;
        let valid = this.state.validAccountName;
        if (!WalletDb.getWallet()) {
            valid = valid && this.state.validPassword;
        }
        if (!firstAccount) {
            valid = valid && this.state.registrar_account;
        }
        return (
            valid &&
            this.state.understand_1 &&
            this.state.understand_2 &&
            this.state.understand_3 &&
            this.state.understand_4 &&
            this.state.nickname &&
            this.state.generatedPassword.length > 7 &&
            this.hasNumber(this.state.generatedPassword) &&
            this.hasLowerCase(this.state.generatedPassword) &&
            this.hasUpperCase(this.state.generatedPassword)
        );
    }

    hasNumber(myString) {
        return /\d/.test(myString);
    }

    hasLowerCase(str) {
        return /[a-z]/.test(str);
    }

    hasUpperCase(str) {
        return /[A-Z]/.test(str);
    }

    onAccountNameChange(e) {
        const state = {};
        if (e.valid !== undefined) state.validAccountName = e.valid;
        if (e.value !== undefined) state.accountName = e.value;
        if (!this.state.show_identicon) state.show_identicon = true;
        this.setState(state);
    }

    onFinishConfirm(confirm_store_state) {
        if (
            confirm_store_state.included &&
            confirm_store_state.broadcasted_transaction
        ) {
            TransactionConfirmStore.unlisten(this.onFinishConfirm);
            TransactionConfirmStore.reset();

            FetchChain("getAccount", this.state.accountName, undefined, {
                [this.state.accountName]: true
            }).then(() => {
                this.props.history.push(
                    "/wallet/backup/create?newAccount=true"
                );
            });
        }
    }

    handleChangeCountry = selectedOptionCountry => {
        this.setState({selectedOptionCountry});
    };

    handleChangeAge = selectedOptionAge => {
        this.setState({selectedOptionAge});
    };

    handleChangeVip = selectedOptionVip => {
        this.setState({selectedOptionVip});
    };

    _unlockAccount(name, password) {
        SettingsActions.changeSetting({
            setting: "passwordLogin",
            value: true
        });

        WalletDb.validatePassword(password, true, name);
        WalletUnlockActions.checkLock.defer();
    }

    createAccount(name, password) {
        let refcode = this.refs.refcode ? this.refs.refcode.value() : null;
        let referralAccount = AccountStore.getState().referralAccount;
        this.setState({loading: true});

        AccountActions.createAccountWithPassword(
            name,
            password,
            this.state.registrar_account,
            referralAccount || this.state.registrar_account,
            0,
            refcode
        )
            .then(() => {
                // and it goes like this
                (async () => {
                    const rawResponse = await fetch(
                        "https://damp.xov.io/api/account",
                        {
                            method: "POST",
                            headers: {
                                Accept: "application/json, text/plain, */*",
                                "Content-Type": "application/json",
                                "X-Requested-With": "XMLHttpRequest",
                                "Access-Control-Allow-Origin":
                                    "https://damp.xov.io"
                            },
                            body: JSON.stringify({
                                account: {
                                    country: this.state.selectedOptionCountry
                                        .label,
                                    accountName: this.state.accountName,
                                    firstname: this.state.firstname,
                                    surname: this.state.surname,
                                    age: this.state.selectedOptionAge.value,
                                    vip: this.state.selectedOptionVip.value,
                                    nickname: this.state.nickname
                                }
                            })
                        }
                    );

                    console.log(rawResponse);
                })();

                AccountActions.setPasswordAccount(name);
                // User registering his own account
                if (this.state.registrar_account) {
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2,
                            loading: false
                        });
                        this._unlockAccount(name, password);
                    });
                    TransactionConfirmStore.listen(this.onFinishConfirm);
                } else {
                    // Account registered by the faucet
                    FetchChain("getAccount", name, undefined, {
                        [name]: true
                    }).then(() => {
                        this.setState({
                            step: 2
                        });
                        this._unlockAccount(name, password);
                    });
                }
            })
            .catch(error => {
                console.log("ERROR AccountActions.createAccount", error);
                let error_msg =
                    error.base && error.base.length && error.base.length > 0
                        ? error.base[0]
                        : "unknown error";
                if (error.remote_ip) error_msg = error.remote_ip[0];
                notify.addNotification({
                    message: `Failed to create account: ${name} - ${error_msg}`,
                    level: "error",
                    autoDismiss: 10
                });
                this.setState({loading: false});
            });
    }

    onSubmit(e) {
        e.preventDefault();

        if (!this.isValid()) return;
        let account_name = this.accountNameInput.getValue();
        // if (WalletDb.getWallet()) {
        //     this.createAccount(account_name);
        // } else {
        let password = this.state.generatedPassword;

        this.createAccount(account_name, password);
    }

    onRegistrarAccountChange(registrar_account) {
        this.setState({registrar_account});
    }

    // showRefcodeInput(e) {
    //     e.preventDefault();
    //     this.setState({hide_refcode: false});
    // }

    onHover(info, condition) {
        this.setState({[info]: condition});
    }

    _onInput(value, e) {
        this.setState({
            [value]:
                value === "confirm_password"
                    ? e.target.value
                    : !this.state[value],
            validPassword:
                value === "confirm_password"
                    ? e.target.value === this.state.generatedPassword
                    : this.state.validPassword
        });
    }

    _onTextInput(value, e) {
        this.setState({
            [value]: e.target.value
        });
    }

    _renderAccountCreateForm() {
        let {registrar_account} = this.state;

        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;
        let valid = this.isValid();
        let isLTM = false;
        let registrar = registrar_account
            ? ChainStore.getAccount(registrar_account)
            : null;
        if (registrar) {
            if (registrar.get("lifetime_referrer") == registrar.get("id")) {
                isLTM = true;
            }
        }

        let buttonClass = classNames("submit-button button no-margin", {
            disabled: !valid || (registrar_account && !isLTM)
        });
        return (
            <div>
                <div
                    style={{
                        display: "inline-block",
                        color: "#3071b3",
                        fontSize: "36px",
                        marginTop: "-40px"
                    }}
                >
                    Create Your Login
                </div>
                <div>
                    <form
                        style={{maxWidth: "60rem"}}
                        onSubmit={this.onSubmit.bind(this)}
                        noValidate
                    >
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "-5px"
                            }}
                        >
                            Account name
                        </div>
                        <div style={{display: "flex"}}>
                            <AccountNameInput
                                ref={ref => {
                                    if (ref) {
                                        this.accountNameInput =
                                            ref.refs.nameInput;
                                    }
                                }}
                                cheapNameOnly={!!firstAccount}
                                onChange={this.onAccountNameChange.bind(this)}
                                accountShouldNotExist={true}
                                noLabel
                            />
                            <div style={{marginTop: "15px"}}>
                                <div
                                    onMouseEnter={this.onHover.bind(
                                        this,
                                        "infoName",
                                        true
                                    )}
                                >
                                    <Icon size="1_5x" name="info-blue" />
                                </div>
                                {this.state.infoName && (
                                    <div
                                        style={{
                                            marginLeft: "32px",
                                            marginTop: "-50px",
                                            position: "fixed",
                                            zIndex: 10,
                                            color: "white",
                                            backgroundColor:
                                                "rgba(48,113,179, 0.5)",
                                            padding: "10px",
                                            borderRadius: "10px",
                                            width: "400px",
                                            height: "100px"
                                        }}
                                        onMouseLeave={() =>
                                            this.setState({infoName: false})
                                        }
                                    >
                                        <div style={{float: "left"}}>
                                            <Icon
                                                size="1_5x"
                                                name="info-filled"
                                            />
                                        </div>
                                        <div
                                            style={{
                                                padding: "10px 10px 10px 30px",
                                                textAlign: "left"
                                            }}
                                        >
                                            The account name is a unique name to
                                            identify your XOV account. The
                                            account name must contain a minimum
                                            of 4 letters and at least one number
                                            and a dash.
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        {/*
                    <section className="form-group">
                        <label className="left-label">
                            <Translate content="wallet.generated" />
                            &nbsp;&nbsp;
                            <span
                                className="tooltip"
                                data-html={true}
                                data-tip={counterpart.translate(
                                    "tooltip.generate"
                                )}
                            >
                                <Icon
                                    name="question-circle"
                                    title="icons.question_circle"
                                />
                            </span>
                        </label>
                        <div style={{paddingBottom: "0.5rem"}}>
                            <span className="inline-label">
                                <textarea
                                    style={{
                                        padding: "0px",
                                        marginBottom: "0px"
                                    }}
                                    rows="3"
                                    readOnly
                                    disabled
                                >
                                    {this.state.generatedPassword}
                                </textarea>
                                <CopyButton
                                    text={this.state.generatedPassword}
                                    tip="tooltip.copy_password"
                                    dataPlace="top"
                                />
                            </span>
                        </div>
                    </section>
                    */}

                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "-15px",
                                marginBottom: "10px"
                            }}
                        >
                            Account nickname
                        </div>
                        <div style={{display: "flex"}}>
                            <input
                                style={{
                                    maxWidth: "238px",
                                    backgroundColor: "#3071b3",
                                    color: "white"
                                }}
                                type="text"
                                name="firstname"
                                id="firstname"
                                value={this.state.nickname}
                                onChange={this._onTextInput.bind(
                                    this,
                                    "nickname"
                                )}
                            />
                            <div
                                onMouseEnter={this.onHover.bind(
                                    this,
                                    "infoNick",
                                    true
                                )}
                            >
                                <Icon size="1_5x" name="info-blue" />
                            </div>
                            {this.state.infoNick && (
                                <div
                                    style={{
                                        marginLeft: "270px",
                                        position: "fixed",
                                        zIndex: 10,
                                        color: "white",
                                        backgroundColor:
                                            "rgba(48,113,179, 0.5)",
                                        padding: "10px",
                                        borderRadius: "10px",
                                        width: "400px",
                                        height: "100px"
                                    }}
                                    onMouseLeave={() =>
                                        this.setState({infoNick: false})
                                    }
                                >
                                    <div style={{float: "left"}}>
                                        <Icon size="1_5x" name="info-filled" />
                                    </div>
                                    <div
                                        style={{
                                            padding: "10px 10px 10px 30px",
                                            textAlign: "left"
                                        }}
                                    >
                                        The account nickname is a friendly name
                                        you can assign to your account that will
                                        link your account name.
                                    </div>
                                </div>
                            )}
                        </div>
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "10px"
                            }}
                        >
                            Create a Password
                        </div>
                        <div style={{display: "flex"}}>
                            <input
                                style={{
                                    maxWidth: "238px",
                                    backgroundColor: "#3071b3",
                                    color: "white"
                                }}
                                type="text"
                                name="surname"
                                id="surname"
                                value={this.state.generatedPassword}
                                onChange={this._onTextInput.bind(
                                    this,
                                    "generatedPassword"
                                )}
                            />

                            <div
                                onMouseEnter={this.onHover.bind(
                                    this,
                                    "infoPass",
                                    true
                                )}
                            >
                                <Icon size="1_5x" name="info-blue" />
                            </div>

                            {this.state.infoPass && (
                                <div
                                    style={{
                                        marginLeft: "270px",
                                        marginTop: "23px",
                                        position: "fixed",
                                        zIndex: 10,
                                        color: "white",
                                        backgroundColor:
                                            "rgba(48,113,179, 0.5)",
                                        padding: "10px",
                                        borderRadius: "10px",
                                        width: "400px",
                                        height: "100px"
                                    }}
                                    onMouseLeave={() =>
                                        this.setState({infoPass: false})
                                    }
                                >
                                    <div style={{float: "left"}}>
                                        <Icon size="1_5x" name="info-filled" />
                                    </div>
                                    <div
                                        style={{
                                            padding: "10px 10px 10px 30px",
                                            textAlign: "left"
                                        }}
                                    >
                                        The password must contain at least 7
                                        characters. The password must include at
                                        least one lowercase letter, one
                                        uppercase letter and a number.
                                    </div>
                                </div>
                            )}
                        </div>
                        {this.state.generatedPassword.length > 7 &&
                        this.hasNumber(this.state.generatedPassword) &&
                        this.hasLowerCase(this.state.generatedPassword) &&
                        this.hasUpperCase(this.state.generatedPassword) ? (
                            <div />
                        ) : (
                            <div
                                style={{
                                    color: "#af2b2c",
                                    fontSize: "12px",
                                    textAlign: "left",
                                    width: "238px"
                                }}
                            >
                                Password should contain at least 7 characters
                                and include Upper-Lower case letters with a
                                number
                            </div>
                        )}
                        {/*
                        <div style={{display: "flex", flexDirection: "column"}}>
                            <Link
                                className="button primary"
                                style={{
                                    backgroundColor: "#BFC0C2",
                                    height: "2.4rem",
                                    color: "white",
                                    width: "150px",
                                    borderRadius: "11px",
                                    marginTop: "20px",
                                    marginBottom: "10px",
                                    float: "left"
                                }}
                                to="/create-account/wallet"
                            >
                                Save Login
                            </Link>
                            
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#3071b3",
                                    marginBottom: "40px"
                                }}
                            >
                                This facility saves an encrypted copy of your
                                login details for future recovery and should be
                                stored in a secure on your personal computer.
                            </div>
                        </div>
                        */}
                        <br />
                        <div
                            className="confirm-checks"
                            onClick={this._onInput.bind(this, "understand_3")}
                        >
                            <label
                                htmlFor="checkbox-1"
                                style={{position: "relative"}}
                            >
                                <input
                                    type="checkbox"
                                    id="checkbox-1"
                                    onChange={() => {}}
                                    checked={this.state.understand_3}
                                    style={{
                                        position: "absolute",
                                        top: "-5px",
                                        left: "0"
                                    }}
                                />
                                <div style={{paddingLeft: "30px"}}>
                                    <div
                                        style={{
                                            textTransform: "none",
                                            transform: "translate(0%,-15%)"
                                        }}
                                    >
                                        I declare that I am authorised to open
                                        an XOV account .
                                    </div>
                                </div>
                            </label>
                        </div>
                        <br />
                        <div
                            className="confirm-checks"
                            onClick={this._onInput.bind(this, "understand_1")}
                        >
                            <label
                                htmlFor="checkbox-2"
                                style={{position: "relative"}}
                            >
                                <input
                                    type="checkbox"
                                    id="checkbox-2"
                                    onChange={() => {}}
                                    checked={this.state.understand_1}
                                    style={{
                                        position: "absolute",
                                        top: "-5px",
                                        left: "0"
                                    }}
                                />
                                <div style={{paddingLeft: "30px"}}>
                                    <div
                                        style={{
                                            textTransform: "none",
                                            transform: "translate(0%,-15%)"
                                        }}
                                    >
                                        I declare that I am solely responsible
                                        for the conduct of my account.
                                    </div>
                                </div>
                            </label>
                        </div>
                        <br />

                        <div
                            className="confirm-checks"
                            style={{paddingBottom: "1.5rem"}}
                            onClick={this._onInput.bind(this, "understand_2")}
                        >
                            <label
                                htmlFor="checkbox-3"
                                style={{position: "relative"}}
                            >
                                <input
                                    type="checkbox"
                                    id="checkbox-3"
                                    onChange={() => {}}
                                    checked={this.state.understand_2}
                                    style={{
                                        position: "absolute",
                                        top: "-5px",
                                        left: "0"
                                    }}
                                />
                                <div style={{paddingLeft: "30px"}}>
                                    <div
                                        style={{
                                            textTransform: "none",
                                            transform: "translate(0%,-15%)"
                                        }}
                                    >
                                        I declare that I am aware of the risks
                                        associated with trading or use of
                                        digital assets.
                                    </div>
                                </div>
                            </label>
                        </div>

                        <div
                            className="confirm-checks"
                            style={{paddingBottom: "1.5rem"}}
                            onClick={this._onInput.bind(this, "understand_4")}
                        >
                            <label
                                htmlFor="checkbox-4"
                                style={{position: "relative"}}
                            >
                                <input
                                    type="checkbox"
                                    id="checkbox-4"
                                    onChange={() => {}}
                                    checked={this.state.understand_4}
                                    style={{
                                        position: "absolute",
                                        top: "-5px",
                                        left: "0"
                                    }}
                                />
                                <div style={{paddingLeft: "30px"}}>
                                    <div
                                        style={{
                                            textTransform: "none",
                                            transform: "translate(0%,-15%)"
                                        }}
                                    >
                                        I declare that all of the answers
                                        provided in my application are correct
                                        and truthful.
                                    </div>
                                </div>
                            </label>
                        </div>
                        {/* If this is not the first account, show dropdown for fee payment account */}
                        {firstAccount ? null : (
                            <div
                                className="full-width-content form-group no-overflow"
                                style={{paddingTop: 30}}
                            >
                                <label>
                                    <Translate content="account.pay_from" />
                                </label>
                                <AccountSelect
                                    account_names={my_accounts}
                                    onChange={this.onRegistrarAccountChange.bind(
                                        this
                                    )}
                                />
                                {registrar_account && !isLTM ? (
                                    <div
                                        style={{textAlign: "left"}}
                                        className="facolor-error"
                                    >
                                        <Translate content="wallet.must_be_ltm" />
                                    </div>
                                ) : null}
                            </div>
                        )}

                        {/* Submit button */}
                        {this.state.loading ? (
                            <LoadingIndicator type="three-bounce" />
                        ) : (
                            <button
                                style={{
                                    borderRadius: "11px",
                                    width: "238px",
                                    height: "2.4rem",
                                    padding: "0px",
                                    marginLeft: "auto"
                                }}
                                className={buttonClass}
                            >
                                <Translate content="account.create_account" />
                            </button>
                        )}

                        {/* Backup restore option */}
                        {/* <div style={{paddingTop: 40}}>
                    <label>
                        <Link to="/existing-account">
                            <Translate content="wallet.restore" />
                        </Link>
                    </label>

                    <label>
                        <Link to="/create-wallet-brainkey">
                            <Translate content="settings.backup_brainkey" />
                        </Link>
                    </label>
                </div> */}

                        {/* Skip to step 3 */}
                        {/* {(!hasWallet || firstAccount ) ? null :<div style={{paddingTop: 20}}>
                    <label>
                        <a onClick={() => {this.setState({step: 3});}}><Translate content="wallet.go_get_started" /></a>
                    </label>
                </div>} */}
                    </form>
                    {/* <br />
                <p>
                    <Translate content="wallet.bts_rules" unsafe />
                </p> */}
                </div>
            </div>
        );
    }

    _renderAccountCreateText() {
        let my_accounts = AccountStore.getMyAccounts();
        let firstAccount = my_accounts.length === 0;

        return (
            <div>
                <h4
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal",
                        paddingBottom: 15
                    }}
                >
                    <Translate content="wallet.wallet_password" />
                </h4>

                <Translate
                    style={{textAlign: "left"}}
                    unsafe
                    component="p"
                    content="wallet.create_account_password_text"
                />

                <Translate
                    style={{textAlign: "left"}}
                    component="p"
                    content="wallet.create_account_text"
                />

                {firstAccount ? null : (
                    <Translate
                        style={{textAlign: "left"}}
                        component="p"
                        content="wallet.not_first_account"
                    />
                )}
            </div>
        );
    }

    _renderBackup() {
        return (
            <div className="backup-submit">
                <p>
                    <Translate unsafe content="wallet.password_crucial" />
                </p>

                <div>
                    {!this.state.showPass ? (
                        <div
                            onClick={() => {
                                this.setState({showPass: true});
                            }}
                            className="button"
                        >
                            <Translate content="wallet.password_show" />
                        </div>
                    ) : (
                        <div>
                            <h5>
                                <Translate content="settings.password" />:
                            </h5>
                            <p
                                style={{
                                    fontWeight: "normal",
                                    fontFamily:
                                        "Roboto-Medium, arial, sans-serif",
                                    fontStyle: "normal",
                                    textAlign: "center"
                                }}
                            >
                                {this.state.generatedPassword}
                            </p>
                        </div>
                    )}
                </div>
                <div className="divider" />
                <p className="txtlabel warning">
                    <Translate unsafe content="wallet.password_lose_warning" />
                </p>

                <div
                    style={{width: "100%"}}
                    onClick={() => {
                        this.props.history.push("/");
                    }}
                    className="button"
                >
                    <Translate content="wallet.ok_done" />
                </div>
            </div>
        );
    }

    _renderGetStarted() {
        return (
            <div>
                <table className="table">
                    <tbody>
                        <tr>
                            <td>
                                <Translate content="wallet.tips_dashboard" />:
                            </td>
                            <td>
                                <Link to="/">
                                    <Translate content="header.dashboard" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_account" />:
                            </td>
                            <td>
                                <Link
                                    to={`/account/${
                                        this.state.accountName
                                    }/overview`}
                                >
                                    <Translate content="wallet.link_account" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_deposit" />:
                            </td>
                            <td>
                                <Link to="/deposit-withdraw">
                                    <Translate content="wallet.link_deposit" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_transfer" />:
                            </td>
                            <td>
                                <Link to="/transfer">
                                    <Translate content="wallet.link_transfer" />
                                </Link>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <Translate content="wallet.tips_settings" />:
                            </td>
                            <td>
                                <Link to="/settings">
                                    <Translate content="header.settings" />
                                </Link>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    _renderGetStartedText() {
        return (
            <div>
                <p
                    style={{
                        fontWeight: "normal",
                        fontFamily: "Roboto-Medium, arial, sans-serif",
                        fontStyle: "normal"
                    }}
                >
                    <Translate content="wallet.congrat" />
                </p>

                <p>
                    <Translate content="wallet.tips_explore_pass" />
                </p>

                <p>
                    <Translate content="wallet.tips_header" />
                </p>

                <p className="txtlabel warning">
                    <Translate content="wallet.tips_login" />
                </p>
            </div>
        );
    }

    render() {
        let {
            step,
            selectedOptionCountry,
            selectedOptionAge,
            selectedOptionVip
        } = this.state;
        // let my_accounts = AccountStore.getMyAccounts();
        // let firstAccount = my_accounts.length === 0;
        if (
            this.state.selectedOptionAge.value &&
            this.state.selectedOptionVip &&
            this.state.selectedOptionCountry.value &&
            this.state.firstname &&
            this.state.surname
        )
            this.setState({isNextStep: true});
        else this.setState({isNextStep: false});
        return (
            <div>
                {this.state.currentStep === 2 && (
                    <div
                        className="sub-content"
                        id="scrollToInput"
                        name="scrollToInput"
                    >
                        <div>
                            {step === 2 ? (
                                <p
                                    style={{
                                        fontWeight: "normal",
                                        fontFamily:
                                            "Roboto-Medium, arial, sans-serif",
                                        fontStyle: "normal"
                                    }}
                                >
                                    <Translate
                                        content={"wallet.step_" + step}
                                    />
                                </p>
                            ) : null}

                            {step === 3 ? this._renderGetStartedText() : null}

                            {step === 1 ? (
                                <div>{this._renderAccountCreateForm()}</div>
                            ) : step === 2 ? (
                                this._renderBackup()
                            ) : (
                                this._renderGetStarted()
                            )}
                        </div>
                    </div>
                )}
                {this.state.currentStep === 1 && (
                    <form>
                        <div
                            style={{
                                display: "inline-block",
                                color: "#3071b3",
                                fontSize: "36px",
                                marginLeft: "-22px"
                            }}
                        >
                            Create Your XOV Account
                        </div>
                        <br />
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "flex-start"
                            }}
                        >
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#3071b3",
                                    marginTop: "15px"
                                }}
                            >
                                XOV provides its customers with a secure
                                environment in which to operate with digital
                                assets. The data we collect from you is used to
                                improve your experience of our products.
                            </div>
                            <div
                                style={{marginTop: "44px", marginRight: "10px"}}
                            >
                                <div
                                    onMouseEnter={this.onHover.bind(
                                        this,
                                        "infoDescription",
                                        true
                                    )}
                                >
                                    <Icon size="1_5x" name="info-blue" />
                                </div>
                                {this.state.infoDescription && (
                                    <div
                                        style={{
                                            marginLeft: "30px",
                                            marginTop: "-38px",
                                            position: "fixed",
                                            color: "white",
                                            backgroundColor:
                                                "rgba(48,113,179, 0.5)",
                                            padding: "10px",
                                            borderRadius: "10px",
                                            width: "400px",
                                            height: "100px"
                                        }}
                                        onMouseLeave={() =>
                                            this.setState({
                                                infoDescription: false
                                            })
                                        }
                                    >
                                        <div style={{float: "left"}}>
                                            <Icon
                                                size="1_5x"
                                                name="info-filled"
                                            />
                                        </div>
                                        <div
                                            style={{
                                                padding: "10px 10px 10px 30px",
                                                textAlign: "left"
                                            }}
                                        >
                                            How we record, use and store your
                                            personal data safely is important to
                                            us. Please go to the XOV website or
                                            find out more about our data
                                            protection policy.
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <br />
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "10px"
                            }}
                        >
                            Please tell us which country you reside in.
                        </div>
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "flex-start"
                            }}
                        >
                            <Select
                                styles={selectorStyles}
                                isSearchable={false}
                                value={selectedOptionCountry}
                                onChange={this.handleChangeCountry}
                                options={optionsCountry}
                            />
                            <div
                                onMouseEnter={this.onHover.bind(
                                    this,
                                    "infoCountry",
                                    true
                                )}
                            >
                                <Icon size="1_5x" name="info-blue" />
                            </div>
                            {this.state.infoCountry && (
                                <div
                                    style={{
                                        marginLeft: "270px",
                                        position: "fixed",
                                        color: "white",
                                        backgroundColor:
                                            "rgba(48,113,179, 0.5)",
                                        padding: "10px",
                                        borderRadius: "10px",
                                        width: "400px",
                                        height: "100px"
                                    }}
                                    onMouseLeave={() =>
                                        this.setState({infoCountry: false})
                                    }
                                >
                                    <div style={{float: "left"}}>
                                        <Icon size="1_5x" name="info-filled" />
                                    </div>
                                    <div
                                        style={{
                                            padding: "10px 10px 10px 30px",
                                            textAlign: "left"
                                        }}
                                    >
                                        We can only offer a full XOV account to
                                        citizens of specified countries at the
                                        moment. We will add more availability
                                        over the coming months.
                                    </div>
                                </div>
                            )}
                        </div>
                        {this.state.selectedOptionCountry.value == "36" ||
                        this.state.selectedOptionCountry.value == "187" ? (
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#ea340b",
                                    marginTop: "10px"
                                }}
                            >
                                Sorry, we can only open a limited account for
                                citizens of your country.
                            </div>
                        ) : (
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#258a14",
                                    marginTop: "10px"
                                }}
                            >
                                Congratulations, we are able to open a full
                                account for you.
                            </div>
                        )}
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "10px"
                            }}
                        >
                            Your First Name
                        </div>
                        <input
                            required
                            style={{
                                maxWidth: "238px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="firstname"
                            id="firstname"
                            value={this.state.firstname}
                            onChange={this._onTextInput.bind(this, "firstname")}
                        />
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "10px"
                            }}
                        >
                            Your Surname
                        </div>
                        <input
                            required
                            style={{
                                maxWidth: "238px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="surname"
                            id="surname"
                            value={this.state.surname}
                            onChange={this._onTextInput.bind(this, "surname")}
                        />
                        <div
                            style={{
                                textAlign: "left",
                                color: "#3071b3",
                                marginTop: "15px",
                                marginBottom: "10px"
                            }}
                        >
                            What year were you born?
                        </div>
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "flex-start"
                            }}
                        >
                            <Select
                                styles={selectorStyles}
                                isSearchable={false}
                                value={selectedOptionAge}
                                onChange={this.handleChangeAge}
                                options={optionsAge}
                            />
                            <div
                                onMouseEnter={this.onHover.bind(
                                    this,
                                    "infoBorn",
                                    true
                                )}
                            >
                                <Icon size="1_5x" name="info-blue" />
                            </div>
                            {this.state.infoBorn && (
                                <div
                                    style={{
                                        marginLeft: "270px",
                                        marginTop: "-70px",
                                        position: "fixed",
                                        zIndex: 10,
                                        color: "white",
                                        backgroundColor:
                                            "rgba(48,113,179, 0.5)",
                                        padding: "10px",
                                        borderRadius: "10px",
                                        width: "400px",
                                        height: "100px"
                                    }}
                                    onMouseLeave={() =>
                                        this.setState({infoBorn: false})
                                    }
                                >
                                    <div style={{float: "left"}}>
                                        <Icon size="1_5x" name="info-filled" />
                                    </div>
                                    <div
                                        style={{
                                            padding: "10px 10px 10px 30px",
                                            textAlign: "left"
                                        }}
                                    >
                                        We can only offer an XOV account to
                                        people aged over 21 years.
                                    </div>
                                </div>
                            )}
                        </div>
                        {this.state.selectedOptionAge.value >
                            new Date().getFullYear() - 21 && (
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#ea340b",
                                    marginTop: "10px"
                                }}
                            >
                                Sorry, we cannot open your account until you
                                reach your 21st birthday
                            </div>
                        )}
                        <div
                            style={{
                                display: "flex",
                                justifyContent: "flex-start"
                            }}
                        >
                            <div
                                style={{
                                    textAlign: "left",
                                    color: "#3071b3",
                                    marginTop: "15px",
                                    marginBottom: "10px"
                                }}
                            >
                                Would you like to upgrade to a VIP account?
                            </div>
                            <div style={{marginTop: "13px"}}>
                                <div
                                    onMouseEnter={this.onHover.bind(
                                        this,
                                        "infoVip",
                                        true
                                    )}
                                >
                                    <Icon size="1_5x" name="info-blue" />
                                </div>
                                {this.state.infoVip && (
                                    <div
                                        style={{
                                            marginLeft: "30px",
                                            marginTop: "-60px",
                                            position: "fixed",
                                            color: "white",
                                            backgroundColor:
                                                "rgba(48,113,179, 0.5)",
                                            padding: "10px",
                                            borderRadius: "10px",
                                            width: "400px",
                                            height: "100px"
                                        }}
                                        onMouseLeave={() =>
                                            this.setState({infoVip: false})
                                        }
                                    >
                                        <div style={{float: "left"}}>
                                            <Icon
                                                size="1_5x"
                                                name="info-filled"
                                            />
                                        </div>
                                        <div
                                            style={{
                                                padding: "10px 10px 10px 30px",
                                                textAlign: "left"
                                            }}
                                        >
                                            VIP accounts provide benefits such
                                            as access to new products early, a
                                            reduction in fees, and discounts.
                                            There is a one time fee of 100 XOV
                                            for this upgrade.
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                        <div style={{width: "238px"}}>
                            <Select
                                styles={selectorStyles}
                                isSearchable={false}
                                value={selectedOptionVip}
                                onChange={this.handleChangeVip}
                                options={optionsVip}
                            />
                        </div>
                        {!this.state.isNextStep && (
                            <div
                                style={{
                                    color: "grey",
                                    float: "right",
                                    cursor: "pointer"
                                }}
                            >
                                Next Page
                            </div>
                        )}

                        {this.state.isNextStep && (
                            <div
                                style={{
                                    color: "#3071b3",
                                    float: "right",
                                    cursor: "pointer"
                                }}
                                onClick={() => {
                                    this.setState({
                                        currentStep: 2
                                    });
                                }}
                            >
                                Next Page
                            </div>
                        )}
                    </form>
                )}
            </div>
        );
    }
}

CreateAccountPassword = withRouter(CreateAccountPassword);

export default connect(
    CreateAccountPassword,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {};
        }
    }
);
