import React, { Component } from "react";
import {RecentTransactions} from "../Account/RecentTransactions";
import Immutable from "immutable";
import Icon from "../Icon/Icon";

class TransactionHistory extends Component {
    constructor(props) {
        super(props);
        this.state = {
            account: this.props.account
        };
    }

    _onTextInput(value, e) {
        this.setState({
          [value]: e.target.value
        });
    }

    render() {
        return (
            <div className="grid-container">
                <div style={{ display: 'flex', justifyContent: 'space-between', width: '95%', margin: '0 auto' }}>
                    <div style={{ fontSize: '28px', color: 'black', margin: '15px 0px 15px 320px', textOverflow: 'nowrap'}}>Last 100 Transactions</div>
                    <div style={{ fontSize: '28px', color: 'white', margin: '15px 0px 15px auto', textOverflow: 'nowrap'}}>Transaction History</div>
                </div>
                <div style={{ backgroundColor: 'rgba(231, 231, 231, 0.5)', width: '80%', marginLeft: '320px', height: '100%', borderRadius: '15px', borderWidth: '2px', padding:'25px' }}>
                    <RecentTransactions
                        accountsList={Immutable.fromJS([
                           this.state.account
                        ])}
                        compactView={false}
                        showMore={true}
                        fullHeight={true}
                        limit={100}
                        showFilters={true}
                        dashboard
                    />
                </div>
            </div>
        );
    }
}

export default TransactionHistory;
