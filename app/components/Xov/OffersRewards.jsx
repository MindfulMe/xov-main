import React, {Component} from "react";
import {getLogo} from "branding";
import AccountStore from "stores/AccountStore";
var logo = getLogo();
import {Link} from "react-router-dom";

class OffersRewards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            limited: false
        };
    }

    _onTextInput(value, e) {
        this.setState({
            [value]: e.target.value
        });
    }

    componentDidMount() {
        (() => {
            fetch("https://damp.xov.io/api/limited", {
                method: "POST",
                headers: {
                    Accept: "application/json, text/plain, */*",
                    "Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "Access-Control-Allow-Origin": "https://damp.xov.io"
                }
            })
                .then(res => res.json())
                .then(response => {
                    console.log(response.accounts);
                    let accountsLength = response.accounts.length;
                    for (var i = 0; i < accountsLength; i++) {
                        if (
                            AccountStore.getState().currentAccount ==
                            response.accounts[i].account_name
                        ) {
                            this.setState({limited: true});
                        }
                    }
                })
                .catch(error => console.log(error));
        })();
    }
    render() {
        return (
            <div className="grid-container">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "95%",
                        margin: "0 auto"
                    }}
                >
                    <div
                        style={{
                            fontSize: "28px",
                            color: "white",
                            margin: "15px 0px 15px auto"
                        }}
                    >
                        Offers and Rewards
                    </div>
                </div>
                {!this.state.limited && (
                    <div
                        style={{
                            display: "grid",
                            gridTemplateColumns: "200px 500px",
                            backgroundColor: "rgba(231, 231, 231, 0.5)",
                            width: "80%",
                            marginLeft: "320px",
                            height: "100%",
                            borderRadius: "15px",
                            borderWidth: "2px",
                            padding: "25px",
                            fontSize: "18px"
                        }}
                    >
                        <div>
                            <img style={{margin: 0, height: 40}} src={logo} />
                            <div style={{margin: "5px 0px 0px -15px"}}>
                                XOV.RST
                            </div>
                        </div>
                        <div>
                            <div>Currently there are no Offers and Rewards</div>
                            <div
                                style={{fontSize: "16px", margin: "20px 0px"}}
                            />
                            {/* <div>
                                <Link
                                    to="/market/XOVRST_BTS"
                                    style={{
                                        backgroundColor: "#3071b3",
                                        height: "2.4rem",
                                        width: "150px",
                                        borderRadius: "11px",
                                        float: "left",
                                        color: "white",
                                        fontSize: "18px",
                                        textAlign: "center",
                                        padding: "10px 0px"
                                    }}
                                >
                                    Exchange XOV
                                </Link>
                            </div> */}
                        </div>
                    </div>
                )}
                <br />
            </div>
        );
    }
}

export default OffersRewards;
