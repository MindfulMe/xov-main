import React, {Component} from "react";
import Select from "react-select";
import AccountStore from "stores/AccountStore";

const selectorStyles = {
    indicatorsContainer: (base, state) => ({
        ...base,
        backgroundColor: "#3071b3",
        color: "white",
        borderRadius: "0 7px 7px 0"
    }),
    indicatorSeparator: (base, state) => ({
        ...base,
        width: 0
    }),
    valueContainer: (base, state) => ({
        ...base,
        backgroundColor: "#3071b3",
        height: "37px",
        minWidth: "180px",
        color: "white",
        borderRadius: "7px 0px 0px 7px"
    }),
    placeholder: (base, state) => ({
        ...base,
        color: "white"
    }),
    singleValue: (base, state) => ({
        ...base,
        color: "white"
    })
};

const optionsCountry = [
    {value: "1", label: "Afghanistan"},
    {value: "2", label: "Albania"},
    {value: "3", label: "Algeria"},
    {value: "4", label: "Andorra"},
    {value: "5", label: "Angola"},
    {value: "6", label: "Antigua & Deps"},
    {value: "7", label: "Argentina"},
    {value: "8", label: "Armenia"},
    {value: "9", label: "Australia"},
    {value: "10", label: "Austria"},
    {value: "11", label: "Azerbaijan"},
    {value: "12", label: "Bahamas"},
    {value: "13", label: "Bahrain"},
    {value: "14", label: "Bangladesh"},
    {value: "15", label: "Barbados"},
    {value: "16", label: "Belarus"},
    {value: "17", label: "Belgium"},
    {value: "18", label: "Belize"},
    {value: "19", label: "Benin"},
    {value: "20", label: "Bhutan"},
    {value: "21", label: "Bolivia"},
    {value: "22", label: "Bosnia Herzegovina"},
    {value: "23", label: "Botswana"},
    {value: "24", label: "Brazil"},
    {value: "25", label: "Brunei"},
    {value: "26", label: "Bulgaria"},
    {value: "27", label: "Burkina"},
    {value: "28", label: "Burundi"},
    {value: "29", label: "Cambodia"},
    {value: "30", label: "Cameroon"},
    {value: "31", label: "Canada"},
    {value: "32", label: "Cape Verde"},
    {value: "33", label: "Central African Rep"},
    {value: "34", label: "Chad"},
    {value: "35", label: "Chile"},
    {value: "36", label: "China"},
    {value: "37", label: "Colombia"},
    {value: "38", label: "Comoros"},
    {value: "39", label: "Congo"},
    {value: "40", label: "Congo {Democratic Rep}"},
    {value: "41", label: "Costa Rica"},
    {value: "42", label: "Croatia"},
    {value: "43", label: "Cuba"},
    {value: "44", label: "Cyprus"},
    {value: "45", label: "Czech Republic"},
    {value: "46", label: "Denmark"},
    {value: "47", label: "Djibouti"},
    {value: "48", label: "Dominica"},
    {value: "49", label: "Dominican Republic"},
    {value: "50", label: "East Timor"},
    {value: "51", label: "Ecuador"},
    {value: "52", label: "Egypt"},
    {value: "53", label: "El Salvador"},
    {value: "54", label: "Equatorial Guinea"},
    {value: "55", label: "Eritrea"},
    {value: "56", label: "Estonia"},
    {value: "57", label: "Ethiopia"},
    {value: "58", label: "Fiji"},
    {value: "59", label: "Finland"},
    {value: "60", label: "France"},
    {value: "61", label: "Gabon"},
    {value: "62", label: "Gambia"},
    {value: "63", label: "Georgia"},
    {value: "64", label: "Germany"},
    {value: "65", label: "Ghana"},
    {value: "66", label: "Greece"},
    {value: "67", label: "Grenada"},
    {value: "68", label: "Guatemala"},
    {value: "69", label: "Guinea"},
    {value: "70", label: "Guinea-Bissau"},
    {value: "71", label: "Guyana"},
    {value: "72", label: "Haiti"},
    {value: "73", label: "Honduras"},
    {value: "74", label: "Hungary"},
    {value: "75", label: "Iceland"},
    {value: "76", label: "India"},
    {value: "77", label: "Indonesia"},
    {value: "78", label: "Iran"},
    {value: "79", label: "Iraq"},
    {value: "80", label: "Ireland {Republic}"},
    {value: "81", label: "Israel"},
    {value: "82", label: "Italy"},
    {value: "83", label: "Ivory Coast"},
    {value: "84", label: "Jamaica"},
    {value: "85", label: "Japan"},
    {value: "86", label: "Jordan"},
    {value: "87", label: "Kazakhstan"},
    {value: "88", label: "Kenya"},
    {value: "89", label: "Kiribati"},
    {value: "90", label: "Korea North"},
    {value: "91", label: "Korea South"},
    {value: "92", label: "Kosovo"},
    {value: "93", label: "Kuwait"},
    {value: "94", label: "Kyrgyzstan"},
    {value: "95", label: "Laos"},
    {value: "96", label: "Latvia"},
    {value: "97", label: "Lebanon"},
    {value: "98", label: "Lesotho"},
    {value: "99", label: "Liberia"},
    {value: "100", label: "Libya"},
    {value: "101", label: "Liechtenstein"},
    {value: "102", label: "Lithuania"},
    {value: "103", label: "Luxembourg"},
    {value: "104", label: "Macedonia"},
    {value: "105", label: "Madagascar"},
    {value: "106", label: "Malawi"},
    {value: "107", label: "Malaysia"},
    {value: "108", label: "Maldives"},
    {value: "109", label: "Mali"},
    {value: "111", label: "Malta"},
    {value: "112", label: "Marshall Islands"},
    {value: "113", label: "Mauritania"},
    {value: "114", label: "Mauritius"},
    {value: "115", label: "Mexico"},
    {value: "116", label: "Micronesia"},
    {value: "117", label: "Moldova"},
    {value: "118", label: "Monaco"},
    {value: "119", label: "Mongolia"},
    {value: "120", label: "Montenegro"},
    {value: "121", label: "Morocco"},
    {value: "122", label: "Mozambique"},
    {value: "123", label: "Myanmar, {Burma}"},
    {value: "124", label: "Namibia"},
    {value: "125", label: "Nauru"},
    {value: "126", label: "Nepal"},
    {value: "127", label: "Netherlands"},
    {value: "128", label: "New Zealand"},
    {value: "129", label: "Nicaragua"},
    {value: "130", label: "Niger"},
    {value: "131", label: "Nigeria"},
    {value: "132", label: "Norway"},
    {value: "133", label: "Oman"},
    {value: "134", label: "Pakistan"},
    {value: "135", label: "Palau"},
    {value: "136", label: "Panama"},
    {value: "137", label: "Papua New Guinea"},
    {value: "138", label: "Paraguay"},
    {value: "139", label: "Peru"},
    {value: "140", label: "Philippines"},
    {value: "141", label: "Poland"},
    {value: "142", label: "Portugal"},
    {value: "143", label: "Qatar"},
    {value: "144", label: "Romania"},
    {value: "145", label: "Russian Federation"},
    {value: "146", label: "Rwanda"},
    {value: "147", label: "St Kitts & Nevis"},
    {value: "148", label: "St Lucia"},
    {value: "149", label: "Saint Vincent & the Grenadines"},
    {value: "150", label: "Samoa"},
    {value: "151", label: "San Marino"},
    {value: "152", label: "Sao Tome & Principe"},
    {value: "153", label: "Saudi Arabia"},
    {value: "154", label: "Senegal"},
    {value: "155", label: "Serbia"},
    {value: "156", label: "Seychelles"},
    {value: "157", label: "Sierra Leone"},
    {value: "158", label: "Singapore"},
    {value: "159", label: "Slovakia"},
    {value: "160", label: "Slovenia"},
    {value: "161", label: "Solomon Islands"},
    {value: "162", label: "Somalia"},
    {value: "163", label: "South Africa"},
    {value: "164", label: "South Sudan"},
    {value: "165", label: "Spain"},
    {value: "166", label: "Sri Lanka"},
    {value: "167", label: "Sudan"},
    {value: "168", label: "Suriname"},
    {value: "169", label: "Swaziland"},
    {value: "170", label: "Sweden"},
    {value: "171", label: "Switzerland"},
    {value: "172", label: "Syria"},
    {value: "173", label: "Tajikistan"},
    {value: "174", label: "Tanzania"},
    {value: "175", label: "Thailand"},
    {value: "176", label: "Togo"},
    {value: "177", label: "Tonga"},
    {value: "178", label: "Trinidad & Tobago"},
    {value: "179", label: "Tunisia"},
    {value: "180", label: "Turkey"},
    {value: "181", label: "Turkmenistan"},
    {value: "182", label: "Tuvalu"},
    {value: "183", label: "Uganda"},
    {value: "184", label: "Ukraine"},
    {value: "185", label: "United Arab Emirates"},
    {value: "186", label: "United Kingdom"},
    {value: "187", label: "United States"},
    {value: "188", label: "Uruguay"},
    {value: "189", label: "Uzbekistan"},
    {value: "190", label: "Vanuatu"},
    {value: "191", label: "Vatican City"},
    {value: "192", label: "Venezuela"},
    {value: "193", label: "Vietnam"},
    {value: "194", label: "Yemen"},
    {value: "195", label: "Zambia"},
    {value: "196", label: "Zimbabwe"}
];

class Profile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            btsid: "",
            email: "",
            birth: "",
            job: "",
            income: "",
            favorite: "",
            telegram: "",
            country: "",
            selectedOptionCountry: {value: "1", label: "Afghanistan"}
        };
    }

    _onTextInput(value, e) {
        this.setState({
            [value]: e.target.value
        });
        localStorage[value] = e.target.value;
        console.log(localStorage[value]);
    }

    handleChangeCountry = selectedOptionCountry => {
        this.setState({selectedOptionCountry});
    };

    componentDidMount() {
        (() => {
            fetch("https://damp.xov.io/api/get-details", {
                method: "POST",
                headers: {
                    Accept: "application/json, text/plain, */*",
                    "Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "Access-Control-Allow-Origin": "https://damp.xov.io"
                }
            })
                .then(res => res.json())
                .then(response => {
                    console.log(response.details);
                    let detailsLength = response.details.length;
                    for (var i = 0; i < detailsLength; i++) {
                        if (
                            AccountStore.getState().currentAccount ==
                            response.details[i].btsid
                        ) {
                            let country = {
                                value: "0",
                                label: response.details[i].country
                            };
                            this.setState({
                                email: response.details[i].email,
                                birth: response.details[i].birth,
                                job: response.details[i].job,
                                income: response.details[i].income,
                                favorite: response.details[i].currency,
                                telegram: response.details[i].telegram,
                                selectedOptionCountry: country
                            });
                        }
                    }
                })
                .catch(error => console.log(error));
        })();
    }

    _onSubmit() {
        (async () => {
            const rawResponse = await fetch("https://damp.xov.io/api/details", {
                method: "POST",
                headers: {
                    Accept: "application/json, text/plain, */*",
                    "Content-Type": "application/json",
                    "X-Requested-With": "XMLHttpRequest",
                    "Access-Control-Allow-Origin": "https://damp.xov.io"
                },
                body: JSON.stringify({
                    details: {
                        btsid: AccountStore.getState().currentAccount,
                        email: this.state.email,
                        birth: this.state.birth,
                        job: this.state.job,
                        income: this.state.income,
                        currency: this.state.favorite,
                        telegram: this.state.telegram,
                        country: this.state.selectedOptionCountry.label
                    }
                })
            });
            console.log(rawResponse);
        })();
        alert("Submitted!");
    }
    render() {
        let {selectedOptionCountry} = this.state;

        return (
            <div className="grid-container">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "95%",
                        margin: "0 auto"
                    }}
                >
                    <div
                        style={{
                            fontSize: "28px",
                            color: "white",
                            margin: "15px 0px 15px auto"
                        }}
                    >
                        Profile
                    </div>
                </div>

                <div
                    style={{
                        backgroundColor: "rgba(231, 231, 231, 0.5)",
                        width: "80%",
                        marginLeft: "320px",
                        height: "100%",
                        borderRadius: "15px",
                        borderWidth: "2px"
                    }}
                >
                    <div
                        style={{
                            display: "grid",
                            gridTemplateColumns: "250px 250px",
                            padding: "25px",
                            gridTemplateRows:
                                "60px 60px 60px 60px 60px 60px 60px"
                        }}
                    >
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: ""
                            }}
                        >
                            Email address
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="email"
                            id="email"
                            value={this.state.email}
                            onChange={this._onTextInput.bind(this, "email")}
                        />
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Date of Birth
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="birth"
                            id="birth"
                            value={this.state.birth}
                            onChange={this._onTextInput.bind(this, "birth")}
                        />
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Occupation/Job
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="job"
                            id="job"
                            value={this.state.job}
                            onChange={this._onTextInput.bind(this, "job")}
                        />
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Annual income ($)
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="income"
                            id="income"
                            value={this.state.income}
                            onChange={this._onTextInput.bind(this, "income")}
                        />

                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Favorite cryptocurrency
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="favorite"
                            id="favorite"
                            value={this.state.favorite}
                            onChange={this._onTextInput.bind(this, "favorite")}
                        />
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Telegram ID
                        </div>
                        <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="telegram"
                            id="telegram"
                            value={this.state.telegram}
                            onChange={this._onTextInput.bind(this, "telegram")}
                        />
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "16px"
                            }}
                        >
                            Country of Residence
                        </div>
                        <Select
                            styles={selectorStyles}
                            isSearchable={false}
                            value={selectedOptionCountry}
                            onChange={this.handleChangeCountry}
                            options={optionsCountry}
                        />
                        {/* <input
                            style={{
                                maxWidth: "250px",
                                backgroundColor: "#3071b3",
                                color: "white"
                            }}
                            type="text"
                            name="country"
                            id="country"
                            value={this.state.country}
                            onChange={this._onTextInput.bind(this, "country")}
                        /> */}
                    </div>
                    <div
                        style={{
                            fontSize: "16px",
                            margin: "10px 0px 150px 25px"
                        }}
                    >
                        Once your profile has been verified we will credit your
                        account with 100 XOV.
                    </div>
                    <div>
                        <button
                            style={{
                                backgroundColor: "#3071b3",
                                height: "2.4rem",
                                width: "150px",
                                borderRadius: "11px",
                                float: "left",
                                color: "white",
                                fontSize: "18px",
                                margin: "-120px 0px 0px 25px"
                            }}
                            onClick={() => {
                                this._onSubmit();
                            }}
                        >
                            Submit
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default Profile;
