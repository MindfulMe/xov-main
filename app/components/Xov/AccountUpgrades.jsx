import React, {Component} from "react";
import Icon from "../Icon/Icon";
import WithdrawModal from "../Modal/WithdrawModalNew";
import GatewayStore from "stores/GatewayStore";

class AccountUpgrades extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    _onTextInput(value, e) {
        this.setState({
            [value]: e.target.value
        });
    }

    render() {
        return (
            <div className="grid-container">
                <div
                    style={{
                        display: "flex",
                        justifyContent: "space-between",
                        width: "95%",
                        margin: "0 auto"
                    }}
                >
                    <div
                        style={{
                            fontSize: "28px",
                            color: "white",
                            margin: "15px 0px 15px auto"
                        }}
                    >
                        Account Upgrades
                    </div>
                </div>
                <WithdrawModal
                    donation
                    ref="withdraw_modal_new"
                    modalId="withdraw_modal_new"
                    backedCoins={GatewayStore.getState().backedCoins}
                />
                <div
                    style={{
                        backgroundColor: "rgba(231, 231, 231, 0.5)",
                        width: "80%",
                        marginLeft: "320px",
                        height: "100%",
                        borderRadius: "15px",
                        borderWidth: "2px",
                        padding: "25px"
                    }}
                >
                    <div style={{fontSize: "19px", margin: "0px 0px 50px 0px"}}>
                        Any charges will be deducted from your current balances.
                        These are non-refundable.
                    </div>
                    <div
                        style={{
                            display: "grid",
                            gridTemplateColumns: "340px 340px",
                            gridTemplateRows:
                                "60px 60px 60px 60px 60px 60px 60px"
                        }}
                    >
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "18px"
                            }}
                        >
                            VIP package
                            <Icon
                                size="1_5x"
                                name="info-filled"
                                title="VIP package"
                            />
                        </div>
                        <div>
                            <button
                                disabled
                                style={{
                                    backgroundColor: "#BFC0C2",
                                    height: "2.4rem",
                                    width: "180px",
                                    borderRadius: "11px",
                                    float: "left",
                                    color: "white",
                                    fontSize: "18px"
                                }}
                                onClick={() => alert("Sent to XOV!")}
                            >
                                100 XOV
                            </button>
                        </div>
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "19px"
                            }}
                        >
                            Donation to the XOV Charity Fund
                            <Icon
                                size="1_5x"
                                name="info-filled"
                                title="Donation to the XOV Charity Fund"
                            />
                        </div>
                        <div>
                            <button
                                style={{
                                    backgroundColor: "#3071b3",
                                    height: "2.4rem",
                                    width: "180px",
                                    borderRadius: "11px",
                                    float: "left",
                                    color: "white",
                                    fontSize: "18px"
                                }}
                                onClick={() => {
                                    this.refs.withdraw_modal_new.show();
                                }}
                            >
                                0.1 ETH
                            </button>
                        </div>
                        <div
                            style={{
                                color: "#black",
                                lineHeight: "35px",
                                fontSize: "19px"
                            }}
                        >
                            XOV Network Improvement
                            <Icon
                                size="1_5x"
                                name="info-filled"
                                title="XOV Network Improvement"
                            />
                        </div>
                        <div>
                            <button
                                style={{
                                    backgroundColor: "#3071b3",
                                    height: "2.4rem",
                                    width: "180px",
                                    borderRadius: "11px",
                                    float: "left",
                                    color: "white",
                                    fontSize: "18px"
                                }}
                                onClick={() => {
                                    this.refs.withdraw_modal_new.show();
                                }}
                            >
                                0.1 ETH
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default AccountUpgrades;
