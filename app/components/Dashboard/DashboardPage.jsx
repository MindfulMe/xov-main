import React from "react";
import {connect} from "alt-react";
import LoadingIndicator from "../LoadingIndicator";
import LoginSelector from "../LoginSelector";
import AccountStore from "stores/AccountStore";
import SettingsStore from "stores/SettingsStore";
import CopyButton from "../Utility/CopyButton";
import {getDepositAddress} from "common/gatewayMethods";
import OpenledgerGateway from "../DepositWithdraw/OpenledgerGateway";
import XovGateway from "../DepositWithdraw/XovGateway";
import GatewayStore from "stores/GatewayStore";
import {getEos, getBtc, getEth, getLogo} from "branding";

var eos = getEos();
var btc = getBtc();
var eth = getEth();
var logo = getLogo();
class DashboardPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            eosModal: false,
            btcModal: false,
            ethModal: false,
            xovModal: false,
            receive_address_eos: null,
            receive_address_btc: null,
            receive_address_eth: null
        };
    }

    depositAddressEOS() {
        this.setState({eosModal: true});
    }

    depositAddressBTC() {
        this.setState({btcModal: true});
    }

    depositAddressETH() {
        this.setState({ethModal: true});
    }

    depositAddressXOV() {
        this.setState({xovModal: true});
    }

    render() {
        let backedCoinss = GatewayStore.getState().backedCoins.get("OPEN", []);
        let openLedgerGatewayCoins = backedCoinss
            .map(coin => {
                if (coin.name === "BTC") return coin;
            })
            .sort((a, b) => {
                if (a.symbol < b.symbol) return -1;
                if (a.symbol > b.symbol) return 1;
                return 0;
            });

        let openLedgerGatewayCoinsE = backedCoinss
            .map(coin => {
                if (coin.name === "ETH") return coin;
            })
            .sort((a, b) => {
                if (a.symbol < b.symbol) return -1;
                if (a.symbol > b.symbol) return 1;
                return 0;
            });

        let xovGatewayCoins = backedCoinss.map(coin => {
            if (coin.name === "ETH") {
                return coin;
            }
        });

        let {
            myActiveAccounts,
            myHiddenAccounts,
            accountsReady,
            passwordAccount
        } = this.props;

        let account = {
            account: AccountStore.getState().currentAccount,
            get: () => AccountStore.getState().currentAccount
        };

        if (!accountsReady && myActiveAccounts) {
            return <LoadingIndicator />;
        }

        let accountCount =
            myActiveAccounts.size +
            myHiddenAccounts.size +
            (passwordAccount ? 1 : 0);
        if (!accountCount) {
            return <LoginSelector />;
        }

        return (
            <div>
                <div className="grid-block align-center" id="accountForm">
                    <div className="grid-block shrink vertical">
                        {this.state.xovModal && (
                            <div
                                style={{
                                    position: "fixed",
                                    width: "500px",
                                    height: "550px",
                                    backgroundColor: "rgba(0,0,0,0.7)",
                                    margin: "20px 0px 0px -25px"
                                }}
                            >
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                        color: "white"
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: "30px",
                                            padding: "30px"
                                        }}
                                    >
                                        Deposit
                                    </div>
                                    <span
                                        style={{
                                            fontSize: "40px",
                                            padding: "10px"
                                        }}
                                        onClick={() =>
                                            this.setState({xovModal: false})
                                        }
                                    >
                                        X
                                    </span>
                                </div>
                                <img
                                    style={{
                                        height: 60,
                                        margin: "20px auto 50px auto",
                                        transform: "translate(380%, 0%)"
                                    }}
                                    src={logo}
                                />
                                <div
                                    style={{
                                        fontSize: "18px",
                                        color: "white",
                                        margin: "0px 30px 0px 30px"
                                    }}
                                >
                                    Please send XOV to the following address:
                                </div>
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "20px 0px 20px 30px"
                                    }}
                                >
                                    <XovGateway
                                        hideTxt={true}
                                        theCoin={"ETH"}
                                        account={account}
                                        coins={xovGatewayCoins}
                                        provider="openledger"
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 0px 20px 30px"
                                    }}
                                >
                                    Minimum value to deposit should be 1.0
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 30px"
                                    }}
                                >
                                    IMPORTANT: Send only XOV to this deposit
                                    address. Sending less than 1.0 XOV or any
                                    other currency to this address may result in
                                    loss of your deposit.
                                </div>
                            </div>
                        )}
                        {this.state.eosModal && (
                            <div
                                style={{
                                    position: "fixed",
                                    width: "500px",
                                    height: "550px",
                                    backgroundColor: "rgba(0,0,0,0.7)",
                                    margin: "20px 0px 0px -25px"
                                }}
                            >
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                        color: "white"
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: "30px",
                                            padding: "30px"
                                        }}
                                    >
                                        Deposit
                                    </div>
                                    <span
                                        style={{
                                            fontSize: "40px",
                                            padding: "10px"
                                        }}
                                        onClick={() =>
                                            this.setState({eosModal: false})
                                        }
                                    >
                                        X
                                    </span>
                                </div>
                                <img
                                    style={{
                                        height: 60,
                                        margin: "20px auto 50px auto",
                                        transform: "translate(380%, 0%)"
                                    }}
                                    src={eos}
                                />
                                <div
                                    style={{
                                        fontSize: "18px",
                                        color: "white",
                                        margin: "0px 30px 0px 30px"
                                    }}
                                >
                                    Please send EOS to the following address:
                                </div>
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "20px 0px 20px 30px"
                                    }}
                                >
                                    <input
                                        readOnly
                                        style={{height: "35px", width: "300px"}}
                                        value="rudexgateway"
                                    />
                                    <CopyButton
                                        text={"rudexgateway"}
                                        className="colored primary"
                                    />
                                </div>
                                <div
                                    style={{
                                        color: "white",
                                        margin: "20px 0px 20px 30px"
                                    }}
                                >
                                    Memo:
                                </div>
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "0px 0px 20px 30px"
                                    }}
                                >
                                    <input
                                        readOnly
                                        style={{height: "35px", width: "300px"}}
                                        value={`dex:${
                                            AccountStore.getState()
                                                .currentAccount
                                        }`}
                                    />
                                    <CopyButton
                                        text={`dex:${
                                            AccountStore.getState()
                                                .currentAccount
                                        }`}
                                        className="colored primary"
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 0px 20px 30px"
                                    }}
                                >
                                    Minimum value to deposit should be 1.4
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 30px"
                                    }}
                                >
                                    IMPORTANT: Send only EOS to this deposit
                                    address. Sending less than 1.4 EOS or any
                                    other currency to this address may result in
                                    loss of your deposit.
                                </div>
                            </div>
                        )}
                        {this.state.btcModal && (
                            <div
                                style={{
                                    position: "fixed",
                                    width: "500px",
                                    height: "550px",
                                    backgroundColor: "rgba(0,0,0,0.7)",
                                    margin: "20px 0px 0px -25px"
                                }}
                            >
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                        color: "white"
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: "30px",
                                            padding: "30px"
                                        }}
                                    >
                                        Deposit
                                    </div>
                                    <span
                                        style={{
                                            fontSize: "40px",
                                            padding: "10px"
                                        }}
                                        onClick={() =>
                                            this.setState({btcModal: false})
                                        }
                                    >
                                        X
                                    </span>
                                </div>
                                <img
                                    style={{
                                        height: 60,
                                        margin: "20px auto 50px auto",
                                        transform: "translate(380%, 0%)"
                                    }}
                                    src={btc}
                                />
                                <div
                                    style={{
                                        fontSize: "18px",
                                        color: "white",
                                        margin: "0px 30px 0px 30px"
                                    }}
                                >
                                    Please send BTC to the following wallet:
                                </div>
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "20px 0px 20px 30px"
                                    }}
                                >
                                    <OpenledgerGateway
                                        hideTxt={true}
                                        theCoin={"BTC"}
                                        account={account}
                                        coins={openLedgerGatewayCoins}
                                        provider="openledger"
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 0px 20px 30px"
                                    }}
                                >
                                    Minimum value to deposit should be 0.0018
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 30px"
                                    }}
                                >
                                    IMPORTANT: Send only BTC to this deposit
                                    address. Sending less than 0.0018 BTC or any
                                    other currency to this address may result in
                                    loss of your deposit.
                                </div>
                            </div>
                        )}
                        {this.state.ethModal && (
                            <div
                                style={{
                                    position: "fixed",
                                    width: "500px",
                                    height: "550px",
                                    backgroundColor: "rgba(0,0,0,0.7)",
                                    margin: "20px 0px 0px -25px"
                                }}
                            >
                                <div
                                    style={{
                                        display: "flex",
                                        justifyContent: "space-between",
                                        color: "white"
                                    }}
                                >
                                    <div
                                        style={{
                                            fontSize: "30px",
                                            padding: "30px"
                                        }}
                                    >
                                        Deposit
                                    </div>
                                    <span
                                        style={{
                                            fontSize: "40px",
                                            padding: "10px"
                                        }}
                                        onClick={() =>
                                            this.setState({ethModal: false})
                                        }
                                    >
                                        X
                                    </span>
                                </div>
                                <img
                                    style={{
                                        height: 60,
                                        margin: "20px auto 50px auto",
                                        transform: "translate(380%, 0%)"
                                    }}
                                    src={eth}
                                />
                                <div
                                    style={{
                                        fontSize: "18px",
                                        color: "white",
                                        margin: "0px 30px 0px 30px"
                                    }}
                                >
                                    Please send ETH to the following wallet:
                                </div>
                                <div
                                    style={{
                                        display: "flex",
                                        margin: "20px 0px 20px 30px"
                                    }}
                                >
                                    <OpenledgerGateway
                                        hideTxt={true}
                                        theCoin={"ETH"}
                                        account={account}
                                        coins={openLedgerGatewayCoinsE}
                                        provider="openledger"
                                    />
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 0px 20px 30px"
                                    }}
                                >
                                    Minimum value to deposit should be 0.011
                                </div>
                                <div
                                    style={{
                                        fontSize: "16px",
                                        color: "white",
                                        margin: "0px 30px"
                                    }}
                                >
                                    IMPORTANT: Send only ETH to this deposit
                                    address. Sending less than 0.011 ETH or any
                                    other currency to this address may result in
                                    loss of your deposit.
                                </div>
                            </div>
                        )}
                        <div
                            className="grid-content shrink text-center account-creation"
                            style={{
                                marginTop: "5%",
                                paddingLeft: "35px",
                                paddingRight: "35px"
                            }}
                        >
                            <div
                                style={{
                                    display: "inline-block",
                                    color: "#3071b3",
                                    fontSize: "36px",
                                    marginLeft: "-22px"
                                }}
                            >
                                Welcome to XOV
                            </div>
                            <div
                                style={{
                                    color: "#3071b3",
                                    fontSize: "18px",
                                    margin: "20px 0px 40px 0px"
                                }}
                            >
                                A world of financial opportunity awaits you.
                            </div>
                            <div
                                style={{
                                    color: "#black",
                                    fontSize: "18px",
                                    textAlign: "left",
                                    margin: "0px 10px"
                                }}
                            >
                                Lets start by depositing your favorite digital
                                currency.
                            </div>
                            <div style={{display: "flex", margin: "30px 10px"}}>
                                <img
                                    style={{
                                        margin: "0px 60px 0px 0px",
                                        height: 40
                                    }}
                                    src={logo}
                                />
                                <span
                                    className="button primary"
                                    style={{
                                        borderRadius: "8px",
                                        width: "150px",
                                        backgroundColor: "#3071b3",
                                        borderStyle: "solid",
                                        textTransform: "none",
                                        fontSize: "1.2rem",
                                        padding: "8px"
                                    }}
                                    onClick={this.depositAddressXOV.bind(this)}
                                >
                                    Deposit
                                </span>
                            </div>
                            <div style={{display: "flex", margin: "30px 10px"}}>
                                <img
                                    style={{
                                        margin: "0px 60px 0px 0px",
                                        height: 40
                                    }}
                                    src={eos}
                                />
                                <span
                                    className="button primary"
                                    style={{
                                        borderRadius: "8px",
                                        width: "150px",
                                        backgroundColor: "#3071b3",
                                        borderStyle: "solid",
                                        textTransform: "none",
                                        fontSize: "1.2rem",
                                        padding: "8px"
                                    }}
                                    onClick={this.depositAddressEOS.bind(this)}
                                >
                                    Deposit
                                </span>
                            </div>
                            <div style={{display: "flex", margin: "30px 10px"}}>
                                <img
                                    style={{
                                        margin: "0px 60px 0px 0px",
                                        height: 40
                                    }}
                                    src={btc}
                                />
                                <span
                                    className="button primary"
                                    style={{
                                        borderRadius: "8px",
                                        width: "150px",
                                        backgroundColor: "#3071b3",
                                        borderStyle: "solid",
                                        textTransform: "none",
                                        fontSize: "1.2rem",
                                        padding: "8px"
                                    }}
                                    onClick={this.depositAddressBTC.bind(this)}
                                >
                                    Deposit
                                </span>
                            </div>
                            <div style={{display: "flex", margin: "30px 10px"}}>
                                <img
                                    style={{
                                        margin: "0px 60px 0px 0px",
                                        height: 40
                                    }}
                                    src={eth}
                                />
                                <span
                                    className="button primary"
                                    style={{
                                        borderRadius: "8px",
                                        width: "150px",
                                        backgroundColor: "#3071b3",
                                        borderStyle: "solid",
                                        textTransform: "none",
                                        fontSize: "1.2rem",
                                        padding: "8px"
                                    }}
                                    onClick={this.depositAddressETH.bind(this)}
                                >
                                    Deposit
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    DashboardPage,
    {
        listenTo() {
            return [AccountStore, SettingsStore];
        },
        getProps() {
            let {
                myActiveAccounts,
                myHiddenAccounts,
                passwordAccount,
                accountsLoaded,
                refsLoaded
            } = AccountStore.getState();

            return {
                account: AccountStore.getState().currentAccount,
                myActiveAccounts,
                myHiddenAccounts,
                passwordAccount,
                accountsReady: accountsLoaded && refsLoaded,
                preferredBases: SettingsStore.getState().preferredBases
            };
        }
    }
);
