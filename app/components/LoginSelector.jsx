import React from "react";
import {connect} from "alt-react";
import AccountStore from "stores/AccountStore";
import {Link} from "react-router-dom";
import Translate from "react-translate-component";
import {isIncognito} from "feature_detect";
import SettingsActions from "actions/SettingsActions";
import WalletUnlockActions from "actions/WalletUnlockActions";
import CreateAccount from "./Account/CreateAccount";
import CreateAccountPassword from "./Account/CreateAccountPassword";
import {Route} from "react-router-dom";
import {getWalletName, getLogo} from "branding";
import Icon from "./Icon/Icon";

var logo = getLogo();

class LoginSelector extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            step: 1
        };
    }

    // componentDidUpdate() {
    // const myAccounts = AccountStore.getMyAccounts();

    // use ChildCount to make sure user is on /create-account page except /create-account/*
    // to prevent redirect when user just registered and need to make backup of wallet or password
    // const childCount = React.Children.count(this.props.children);

    // do redirect to portfolio if user already logged in
    // if (
    //     this.props.history &&
    //     Array.isArray(myAccounts) &&
    //     myAccounts.length !== 0 &&
    //     childCount === 0
    // )
    //     this.props.history.push("/account/" + this.props.currentAccount);
    // }

    componentWillMount() {
        isIncognito(incognito => {
            this.setState({incognito});
        });
    }

    onSelect(route) {
        this.props.history.push("/create-account/" + route);
    }

    render() {
        const translator = require("counterpart");

        return (
            <div className="grid-block align-center" id="accountForm">
                <div className="grid-block shrink vertical">
                    {!window.location.href.slice(0, -3).includes("/", 8) && (
                        <div
                            className="grid-content shrink text-center account-creation"
                            style={{marginTop: "50%"}}
                        >
                            <img style={{margin: 0, height: 100}} src={logo} />
                            <div>
                                <Translate
                                    content="account.intro_text_title"
                                    component="h1"
                                    wallet_name={getWalletName()}
                                    style={{
                                        color: "#0671B9",
                                        fontSize: "35px",
                                        marginBottom: "5px"
                                    }}
                                />
                                <Translate
                                    unsafe
                                    content="account.intro_text_1"
                                    component="p"
                                    style={{
                                        color: "#0671B9",
                                        marginBottom: "10px",
                                        marginTop: "10px"
                                    }}
                                />
                            </div>

                            <div className="grid-block account-login-options">
                                <Link
                                    id="account_login_button"
                                    to="/create-account/password"
                                    className="button primary"
                                    data-intro={translator.translate(
                                        "walkthrough.create_cloud_wallet"
                                    )}
                                    style={{
                                        borderRadius: "8px",
                                        backgroundColor: "#1D71B8"
                                    }}
                                >
                                    <Translate
                                        content="header.create_account"
                                        style={{
                                            position: "relative",
                                            top: "-5%"
                                        }}
                                    />
                                </Link>

                                <span
                                    className="button primary"
                                    onClick={() => {
                                        SettingsActions.changeSetting.defer({
                                            setting: "passwordLogin",
                                            value: true
                                        });
                                        WalletUnlockActions.unlock().catch(
                                            () => {}
                                        );
                                    }}
                                    style={{
                                        borderRadius: "8px",
                                        backgroundColor: "#1D71B8",
                                        display: "inline-block",
                                        padding: 0
                                    }}
                                >
                                    <Translate
                                        content="header.unlock_short"
                                        style={{
                                            position: "relative",
                                            top: "-28%",
                                            marginRight: "10px"
                                        }}
                                    />
                                    <Icon
                                        className="lock-unlock"
                                        size="2x"
                                        name="locked"
                                        title="icons.locked.common"
                                    />
                                </span>
                            </div>
                        </div>
                    )}

                    <Route
                        path="/create-account/wallet"
                        exact
                        component={CreateAccount}
                    />
                    {window.location.href.includes(
                        "/create-account/password"
                    ) && (
                        <div className="grid-content shrink text-center account-creation">
                            <Route
                                path="/create-account/password"
                                exact
                                component={CreateAccountPassword}
                            />
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default connect(
    LoginSelector,
    {
        listenTo() {
            return [AccountStore];
        },
        getProps() {
            return {
                currentAccount:
                    AccountStore.getState().currentAccount ||
                    AccountStore.getState().passwordAccount
            };
        }
    }
);
